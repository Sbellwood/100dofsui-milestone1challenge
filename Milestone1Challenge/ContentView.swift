//
//  ContentView.swift
//  Milestone1Challenge
//
//  Created by Skyler Bellwood on 7/4/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    var moves = ["Rock", "Paper", "Scissors"]
    
    @State private var currentChoice = Int.random(in: 0...2)
    @State private var shouldWin = Bool.random()
    @State private var score = 0
    @State private var alertTitle = ""
    @State private var showingAlert = false
    
    var body: some View {
        VStack {
            Text(shouldWin ? "Your task is to win against \(moves[currentChoice])" : "Your task is to lose against \(moves[currentChoice])")
                .font(.largeTitle)
                .multilineTextAlignment(.center)
            Spacer()
            HStack {
                ForEach(moves, id: \.self) { move in
                    Button("\(move)") {
                        self.handleSelection(selection: "\(move)")
                    }
                    .frame(width: 100, height: 65, alignment: .center)
                    .foregroundColor(.white)
                    .background(Color.blue)
                    .clipShape(Capsule())
                }
            }
            Spacer()
            Text("Score: \(score)")
        }
        .alert(isPresented: $showingAlert) {
            Alert(title: Text(alertTitle), message: Text("Your score is: \(score)"), dismissButton: .default(Text("Next Round")) {
                self.startRound()
                } )
        }
    }
    
    func startRound() {
        currentChoice = Int.random(in: 0...2)
        shouldWin = Bool.random()
    }
    
    func handleSelection(selection: String) {
        switch (moves[currentChoice], shouldWin) {
        case ("Rock", true):
            if selection == "Paper" {
                alertTitle = "Correct!"
                score += 100
            } else {
                alertTitle = "Whoops!"
                score -= 100
            }
        case ("Rock", false):
            if selection == "Scissors" {
                alertTitle = "Correct!"
                score += 100
            } else {
                alertTitle = "Whoops!"
                score -= 100
            }
        case ("Paper", true):
            if selection == "Scissors" {
                alertTitle = "Correct!"
                score += 100
            } else {
                alertTitle = "Whoops!"
                score -= 100
            }
        case ("Paper", false):
            if selection == "Rock" {
                alertTitle = "Correct!"
                score += 100
            } else {
                alertTitle = "Whoops!"
                score -= 100
            }
        case ("Scissors", true):
            if selection == "Rock" {
                alertTitle = "Correct!"
                score += 100
            } else {
                alertTitle = "Whoops!"
                score -= 100
            }
        case ("Scissors", false):
            if selection == "Paper" {
                alertTitle = "Correct!"
                score += 100
            } else {
                alertTitle = "Whoops!"
                score -= 100
            }
        default:
            if selection == "Paper" {
                alertTitle = "Correct!"
                score += 100
            } else {
                alertTitle = "Whoops!"
                score -= 100
            }
        }
        showingAlert = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
